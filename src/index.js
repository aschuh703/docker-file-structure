import React from 'react'
// import ReactDOM from 'react-dom';
import {render} from 'react-dom'
import './index.css';
import {Controller} from 'cerebral'
import {Container} from 'cerebral/react'
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const controller = Controller({
    state: {
        foo: 'bar'
    }
});

render((
    <Container controller={controller}>
        <App />
    </Container>
), document.getElementById('root'));

registerServiceWorker();
