import React from 'react';
import { Form, Label, Input, Button, List, Grid, Segment } from 'semantic-ui-react'
import 'jszip'
import 'semantic-ui-css/semantic.min.css';
import Icon from 'semantic-ui-react/dist/es/elements/Icon/Icon';
import './App.css';
import UploadArea from "./UploadArea";

function Process_Directory (structure, visible) {
    return (
        <List>
            {structure.map (function (entry)
               {return [Process_Entry(entry)]}
            )}
        </List>
    )
}

function Process_Directory2 (structure) {
    return (
        <List.List>
            {structure.map (function (entry)
                {return [Process_Entry(entry)]}
            )}
        </List.List>
    )
}

function Process_Entry (entry)
{
    if (typeof entry.content === 'string') {
        return <List.Item>
            <List.Icon name='file' />
            <List.Content>
                <Button onClick={() => handleClick(entry.key)}>
                    {entry.name}
                </Button>
            </List.Content>
        </List.Item>
    } else {
        return <List.Item>
            <Icon name='folder' />
            <List.Content>
                <List.Description>{entry.name}</List.Description>
                    {Process_Directory2(entry.content)}
            </List.Content>
        </List.Item>
    }
}

function ReadFile (fstruct, currentkey) { /*Add Key*/
    var newstruct = [];
    var newentry = [];

    for (let i = 0; i < fstruct.length; i++) {
        newentry = [];
        newentry.key = currentkey;
        newentry.name = fstruct[i].name;
        if (typeof fstruct[i].content === 'string') {
            newentry.content = fstruct[i].content;
        } else {
            newentry.content = ReadFile (fstruct[i].content, currentkey+1);
            currentkey = newentry.content[newentry.content.length-1].key
        }
        newstruct.push(newentry);
        currentkey += 1
    }

    return newstruct
}

function handleClick (key) {
    console.log('A button was clicked: ' + key.toString());
}

function test_zip () {
    var JSZip = require("jszip");
    var zip = new JSZip();

    zip.file("hello.txt", "Hello[p my)6cxsw2q");
    zip.file("hello.txt", "Hello World\n");


    zip.file("nested/hello.txt", "Hello World\n");
    zip.folder("nested").file("hello.txt", "Hello World\n");

    return zip;
}

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            DStruct: ReadFile(dirStructure, 0),
            value: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    read_stage () {
        return this.state.current_stage;
    }

    change_stage (value) {
        //setState(this.state.current_stage = value);
        this.setState({current_stage: value});
    }

    setkey () {
        this.setState({
           keynum: 0
        });
        return this.state.keynum
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        var z = test_zip();

        return (
            <div id="parent">
                <Grid columns='equal'>
                    <Grid.Column>
                        <Segment>
                            Select file to process. <br/>
                            <UploadArea/>
                            {Process_Directory (this.state.DStruct, "False")}
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment>
                            <Form onSubmit={this.handleSubmit}>
                                <Label>
                                    Name:
                                    <Input type="text" value={this.state.value} onChange={this.handleChange} />
                                </Label>
                                <input type="submit" value="Submit" />
                            </Form>
                        </Segment>
                    </Grid.Column>
                </Grid>
                {global.x}
            </div>


        );
    }
}

export default App;

const dirStructure = [
    {
        name: "dir1",
        content: [
            {
                name: "dir2",
                content: [
                    {
                        name: "dir3",
                        content: [
                            {
                                name: "file6",
                                content: "Fdsa"
                            },
                            {
                                name: "file7",
                                content: "fdsa"
                            }
                        ]
                    },
                    {
                        name: "file3",
                        content: "Fdsa"
                    },
                    {
                        name: "file4",
                        content: "fdsa"
                    },
                    {
                        name: "file5",
                        content: "fdsa"
                    }
                ]
            },
            {
                name: "dir4",
                content: [
                    {
                        name: "file8",
                        content: "Fdsa"
                    },
                    {
                        name: "file9",
                        content: "fdsa"
                    }
                ]
            },
            {
                name: "file1",
                content: "fdsafdSA"
            },
            {
                name: "file2",
                content: "FDsafdsa"
            }
        ]
    },
    {
        name: "dir5",
        content: [
            {
                name: "file10",
                content: "Fdsa"
            },
            {
                name: "file11",
                content: "fda"
            }
        ]
    },
    {
        name: "file0",
        content: "Test text."
    }
];