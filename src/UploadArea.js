import React, { Component } from 'react';
import { connect } from 'cerebral/react';
// eslint-disable-next-line
import { Core, DragDrop, Dashboard, Tus10 } from 'uppy';

import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css'

// eslint-disable-next-line import/no-webpack-loader-syntax
import '!style-loader!css-loader!uppy/dist/uppy.min.css';
//import './App';

var App = require ('./App');

export default connect(
    { },
    class UploadArea extends Component {
        constructor() {
            super();
            this.addFile = this.addFile.bind(this);
            // this.upload = this.upload.bind(this);
            this.onFileAdd = this.onFileAdd.bind(this);
            this.fileAdded = this.fileAdded.bind(this);
            this.state = {
                images: [],
                alert: false
            };
        }

        componentWillUnmount() {}
        componentDidMount() {

            this.uppy = new Core({
                debug: true,
                autoProceed: false,
                restrictions: {
                    maxFileSize: 1000000,
                    maxNumberOfFiles: 1,
                    minNumberOfFiles: 1,
                },
            });
            this.uppy
                .use(Dashboard, {
                    target: this.uppyElement,
                    maxHeight: 185,
                    inline: true,
                    locale: {
                        strings: {
                            dropPasteImport:
                                "Drop ZIP file here, or ",
                            dropPaste: "Drop ZIP file here, or "
                        }
                    }
                })
                .use(Tus10, {
                    endpoint: "/api/upload/",
                    resume: true
                })
                .run();

            this.uppy.addFile2 = this.uppy.addFile;
            this.uppy.addFile = this.addFile;

            this.uppy.on("core:file-add", this.onFileAdd);
            this.uppy.on("file-added", this.fileAdded);
        }

        onFileAdd(file) {}
        fileAdded(fileID) {
            console.log("file added ", fileID);
        }
        addFile(file) {
            console.log("file ", file);
            if (file.name.substring(file.name.lastIndexOf('.') + 1) === "zip")
            {
                this.uppy.addFile2({
                source: "React input",
                name: file.name,
                type: file.type,
                alt: file.name,
                data: file.data
                });
            } else {
                this.setState({ alert: true });
            }
        }

        render() {
            return (
                <div style={{ color: "black", minWidth: 0 }}>
                    {this.state.images.map(img => {
                        return <img width="300" src='zip.png' alt={img.alt} />;
                    })}
                    <div
                        ref={node => {
                            this.uppyElement = node;
                        }}
                    />
                    <SweetAlert
                        show={this.state.alert}
                        title="OOPS!"
                        text="Please upload a ZIP file."
                        onConfirm={() => this.setState({ alert: false })}
                    />
                </div>
            );
        }
    }
);